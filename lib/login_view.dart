import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const usernameKey = 'usernameKey';
const passwordKey = 'passwordKey';

class LoginView extends StatefulWidget {
  @override
  LoginViewState createState() => new LoginViewState();
}

class LoginViewState extends State<LoginView> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final FlutterSecureStorage storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    _initFields();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Username & password'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: _onTapBack,
        ),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: TextField(
              controller: _usernameController,
              decoration: InputDecoration(labelText: 'Username'),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: TextField(
              controller: _passwordController,
              decoration: InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
          ),
        ],
      ),
    );
  }

  void _onTapBack() async {
    if (_usernameController.text.isEmpty || _passwordController.text.isEmpty) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text('You must enter your username and password'),
                actions: <Widget>[
                  ElevatedButton(
                    child: Text('Close'),
                    onPressed: () => Navigator.pop(context),
                  )
                ],
              ));
      return;
    }
    await storage.write(key: usernameKey, value: _usernameController.text);
    await storage.write(key: passwordKey, value: _passwordController.text);
    Navigator.of(context).pop();
  }

  _initFields() async {
    _usernameController.text = await storage.read(key: usernameKey) ?? '';
    _passwordController.text = await storage.read(key: passwordKey) ?? '';
  }
}

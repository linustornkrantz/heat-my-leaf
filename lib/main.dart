import 'package:flutter/material.dart';

import 'button_view.dart';

void main() => runApp(MyApp());

const appTitle = 'Leaf Heater';

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: ButtonView(),
    );
  }
}

import 'dart:async';

import 'package:dartcarwings/dartcarwings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'login_view.dart';
import 'main.dart';

class ButtonView extends StatefulWidget {
  @override
  ButtonViewState createState() => ButtonViewState();
}

class ButtonViewState extends State<ButtonView> {
  final _session = new CarwingsSession();
  List<String> _logData = [];
  bool _hasCredentials = false;
  bool _waitingForTap = false;
  final FlutterSecureStorage storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    _checkCredentials();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: _onTapSettings,
          )
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(30),
              child: ElevatedButton(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.whatshot,
                        size: 50,
                      ),
                      Text('Start heater'),
                    ],
                  ),
                ),
                onPressed: _waitingForTap ? _onTapStartHeater : null,
              ),
            ),
            Column(
              children: _logData.map((text) => Text(text)).toList(),
            ),
          ],
        ),
      ),
    );
  }

  Future _onTapStartHeater() async {
    _waitingForTap = false;
    _logData.clear();
    _log('Logging in...');

    final username = await storage.read(key: usernameKey);
    final password = await storage.read(key: passwordKey);
    final userAgent = 'Dalvik/2.1.0 (Linux; U; Android 5.1.1; Android SDK built for x86 Build/LMY48X)';

    if (username == null) {
      _log('Username must be set');
      return;
    }
    if (password == null) {
      _log('Password must be set');
      return;
    }

    try {
      await _session.login(username: username, password: password, region: CarwingsRegion.Europe, userAgent: userAgent
//        blowfishEncryptCallback: (String key, String password) async {
          //        return await BlowfishNative.encrypt(key, password);
          //    },
          );

      _log('Turning on heater...');
      _log('(You may exit the app now)');
      await _session.vehicle.requestClimateControlOn();
      _log('Done');
    } catch (e) {
      if (e == 'Login error') {
        _log('Username or password is incorrect');
      } else {
        _log('Maybe it worked, maybe not');
      }
    }
    setState(() {
      _waitingForTap = true;
    });
  }

  Future _onTapSettings() async {
    await Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginView()));
    _logData.clear();
    _checkCredentials();
  }

  void _log(String text) {
    setState(() {
      _logData.add(text);
    });
  }

  _checkCredentials() async {
    final username = await storage.read(key: usernameKey);
    final password = await storage.read(key: passwordKey);

    setState(() {
      _hasCredentials = (username != null && username.isNotEmpty && password != null && password.isNotEmpty);
      _waitingForTap = true;
    });
    if (!_hasCredentials) {
      await Navigator.push(context, MaterialPageRoute(builder: (context) => LoginView()));
      _logData.clear();
      _checkCredentials();
    }
  }
}

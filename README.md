# Leaf heater

A Flutter app to start your Nissan Leaf air conditioner with just one button. This is much faster than using the official NissanConnect app. 

Based on dartcarwings (https://gitlab.com/tobiaswkjeldsen/dartcarwings). 
